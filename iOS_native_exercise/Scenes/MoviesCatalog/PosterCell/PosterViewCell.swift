//
//  PosterViewCell.swift
//  iOS_native_exercise
//
//  Created by Eduardo Cacao on 03/03/19.
//  Copyright © 2019 Eduardo Cacao. All rights reserved.
//

import UIKit

class PosterViewCell: UICollectionViewCell {

    @IBOutlet weak var imagePoster: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let loadingGif = UIImage.gifImageWithName("loading")
        /*let imageView = UIImageView(image: jeremyGif)
        imageView.frame = CGRect(x: 20.0, y: 50.0, width: self.view.frame.size.width - 40, height: 150.0)
        view.addSubview(imageView)*/
        imagePoster.image = loadingGif
    }
    func setPoster( image: UIImage ){
        imagePoster.contentMode = .scaleToFill
        imagePoster.image = image
    }
    func setLoading( image: UIImage ){
        imagePoster.contentMode = .center
        imagePoster.image = image
    }
}
