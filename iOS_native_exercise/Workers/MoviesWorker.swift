//
//  MoviesWorker.swift
//  iOS_native_exercise
//
//  Created by Eduardo Cacao on 03/03/19.
//  Copyright (c) 2019 Eduardo Cacao. All rights reserved.
//

import UIKit

class MoviesWorker
{
    var movieStore: MovieStoreProtocol
    
    init(movieStore: MovieStoreProtocol){
        self.movieStore = movieStore
    }
    
    
  
}

protocol MovieStoreProtocol {
    func fetchMovies(by: Classification, for page: Int, completionHandler: @escaping ([Movie], MovieStoreError?) -> Void)
    func fetchMovieDetail(id: Int, completionHandler: @escaping (MovieDetails?,MovieStoreError?) -> Void )
    func fetchMoviePoster(path: String, with dimension: PosterDimension, completionHandler: @escaping (UIImage?, MovieStoreError?) -> Void)
}

enum Classification: String {
    case Popular = "popular"
    case TopRated = "top_rated"
}

enum PosterDimension: String{
    case w92
    case w154
    case w185
    case w542
    case w500
    case w780
}

// MARK: - Movies store operation errors

enum MovieStoreError{
    case CannotFetch(String)
    case ServerError(String)
    case CannotAuthorize(String)
    case CannotFound(String)
    case InternalError(String)
}


