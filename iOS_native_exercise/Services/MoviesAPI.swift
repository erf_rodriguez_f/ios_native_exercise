//
//  MoviesAPI.swift
//  iOS_native_exercise
//
//  Created by Eduardo on 02/03/19.
//  Copyright © 2019 Eduardo Cacao. All rights reserved.
//

import UIKit
import Alamofire
class MoviesAPI: MovieStoreProtocol {
    
    let baseAPIURL: String = "https://api.themoviedb.org/"
    let baseImageUrl: String = "https://image.tmdb.org/t/p/"
    let APIRequestPath: String = "3/movie/"
    
    
    func fetchMovies(by: Classification, for page: Int, completionHandler: @escaping ([Movie], MovieStoreError?) -> Void) {
        let langStr = Locale.current.languageCode
        let requestURL = baseAPIURL + APIRequestPath + by.rawValue
        let parameters: Parameters = [
            "api_key": APIKey(for: "MovieDB"),
            "page": String(page),
            "language": langStr!
        ]
        AF.request(requestURL, method: .get, parameters: parameters).validate(statusCode: 200..<299).responseDecodable { (response: DataResponse<MoviesResponse>) in
            switch response.result {
            case .success(let moviesResponse):
                completionHandler(moviesResponse.results, nil)
            case .failure(let error):
                guard let statusCode = error.asAFError?.responseCode else{
                    completionHandler([], MovieStoreError.InternalError(error.localizedDescription))
                    return
                }
                switch statusCode{
                case 401:
                    completionHandler([], MovieStoreError.CannotAuthorize(error.localizedDescription))
                case 404:
                    completionHandler([], MovieStoreError.CannotFound(error.localizedDescription))
                case 500:
                    completionHandler([], MovieStoreError.ServerError(error.localizedDescription))
                default:
                    completionHandler([], MovieStoreError.CannotFetch(error.localizedDescription))
                }
            }
        }
    }
    
    func fetchMovieDetail(id: Int, completionHandler: @escaping (MovieDetails?, MovieStoreError?) -> Void) {
        let langStr = Locale.current.languageCode
        let requestURL = baseAPIURL + APIRequestPath + String(id)
        let parameters: Parameters = [
            "api_key": APIKey(for: "MovieDB"),
            "language": langStr!
        ]
        print(parameters)
        AF.request(requestURL, method: .get, parameters: parameters).validate(statusCode: 200..<299).responseDecodable { (response: DataResponse<MovieDetails>) in
            switch response.result {
            case .success(let movieDetailResponse):
                completionHandler(movieDetailResponse, nil)
            case .failure(let error):
                guard let statusCode = error.asAFError?.responseCode else{
                    completionHandler(nil, MovieStoreError.InternalError(error.localizedDescription))
                    return
                }
                switch statusCode{
                case 401:
                    completionHandler(nil, MovieStoreError.CannotAuthorize(error.localizedDescription))
                case 404:
                    completionHandler(nil, MovieStoreError.CannotFound(error.localizedDescription))
                case 500:
                    completionHandler(nil, MovieStoreError.ServerError(error.localizedDescription))
                default:
                    completionHandler(nil, MovieStoreError.CannotFetch(error.localizedDescription))
                }
            }
        }
        
    }
    
    func fetchMoviePoster(path: String, with dimension: PosterDimension, completionHandler: @escaping (UIImage?, MovieStoreError?) -> Void) {
        let requestURL = baseImageUrl + dimension.rawValue + "/"+path
        AF.request(requestURL, method: .get).validate(statusCode: 200..<299).responseData { (response: DataResponse<Data>) in
            switch response.result {
            case .success(let data):
                if let image = UIImage(data: data,scale: 1){
                    completionHandler(image,nil)
                }
                else{
                    completionHandler(nil,MovieStoreError.InternalError("Could not create UIImage from data"))
                }
            case .failure(let error):
                guard let statusCode = error.asAFError?.responseCode else{
                    completionHandler(nil, MovieStoreError.InternalError(error.localizedDescription))
                    return
                }
                switch statusCode{
                case 401:
                    completionHandler(nil, MovieStoreError.CannotAuthorize(error.localizedDescription))
                case 404:
                    completionHandler(nil, MovieStoreError.CannotFound(error.localizedDescription))
                case 500:
                    completionHandler(nil, MovieStoreError.ServerError(error.localizedDescription))
                default:
                    completionHandler(nil, MovieStoreError.CannotFetch(error.localizedDescription))
                }
            }
        }
        
    }
    
    private func APIKey(for keyname: String) -> String {
        let filePath = Bundle.main.path(forResource: "APIKeys", ofType: "plist")
        let plist = NSDictionary(contentsOfFile: filePath!)
        return plist?.object(forKey: keyname) as! String
    }
    
}
