//
//  MovieDetails.swift
//  iOS_native_exercise
//
//  Created by Eduardo Cacao on 03/03/19.
//  Copyright © 2019 Eduardo Cacao. All rights reserved.
//

struct MovieDetails: Codable {
    let id: Int
    let originalTitle: String
    let posterPath: String
    let overview: String
    let releaseDate: String
    let tagline: String
    let voteAverage: Double
    let runtime: Int
    let title: String
}

extension MovieDetails {
    enum CodingKeys: String, CodingKey {
        case id
        case originalTitle = "original_title"
        case posterPath = "poster_path"
        case overview
        case releaseDate = "release_date"
        case tagline
        case voteAverage = "vote_average"
        case runtime
        case title
    }
}


/*
 {
    "adult": false,
    "backdrop_path": "/6xKCYgH16UuwEGAyroLU6p8HLIn.jpg",
    "belongs_to_collection": {
        "id": 230,
        "name": "The Godfather Collection",
        "poster_path": "/sSbkKCHtIEakht5rnEjrWeR2LLG.jpg",
        "backdrop_path": "/3WZTxpgscsmoUk81TuECXdFOD0R.jpg"
    },
    "budget": 6000000,
    "genres": [
    {
    "id": 18,
    "name": "Drama"
    },
    {
    "id": 80,
    "name": "Crime"
    }
    ],
    "homepage": "http://www.thegodfather.com/",
    "id": 238,
    "imdb_id": "tt0068646",
    "original_language": "en",
    "original_title": "The Godfather",
    "overview": "Spanning the years 1945 to 1955, a chronicle of the fictional Italian-American Corleone crime family. When organized crime family patriarch, Vito Corleone barely survives an attempt on his life, his youngest son, Michael steps in to take care of the would-be killers, launching a campaign of bloody revenge.",
    "popularity": 26.877,
    "poster_path": "/rPdtLWNsZmAtoZl9PK7S2wE3qiS.jpg",
    "production_companies": [
    {
    "id": 4,
    "logo_path": "/fycMZt242LVjagMByZOLUGbCvv3.png",
    "name": "Paramount",
    "origin_country": "US"
    },
    {
    "id": 10211,
    "logo_path": null,
    "name": "Alfran Productions",
    "origin_country": "US"
    }
    ],
    "production_countries": [
    {
    "iso_3166_1": "US",
    "name": "United States of America"
    }
    ],
    "release_date": "1972-03-14",
    "revenue": 245066411,
    "runtime": 175,
    "spoken_languages": [
    {
    "iso_639_1": "en",
    "name": "English"
    },
    {
    "iso_639_1": "it",
    "name": "Italiano"
    },
    {
    "iso_639_1": "la",
    "name": "Latin"
    }
    ],
    "status": "Released",
    "tagline": "An offer you can't refuse.",
    "title": "The Godfather",
    "video": false,
    "vote_average": 8.6,
    "vote_count": 9482
}
*/
