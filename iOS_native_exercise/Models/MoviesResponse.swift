//
//  MoviesResponse.swift
//  iOS_native_exercise
//
//  Created by Eduardo Cacao on 03/03/19.
//  Copyright © 2019 Eduardo Cacao. All rights reserved.
//

import Foundation

struct MoviesResponse: Codable {
    let page: Int
    let totalResults: Int
    let totalPages: Int
    let results: [Movie]
}

extension MoviesResponse {
    enum CodingKeys: String, CodingKey {
        case page
        case totalResults = "total_results"
        case totalPages = "total_pages"
        case results
    }
}

